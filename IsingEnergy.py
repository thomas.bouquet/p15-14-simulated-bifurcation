# Modules importation

import numpy as np
from binaryTool.binaryForm import toBinary
from binaryTool.utility import allBinaryCombinations, binaryToSpin

def IsingEnergy(J,h,s):

    """
    Compute the Ising energy for given matrix J, vector h and a ground-state s.

    J : N x N matrix
    h : N x 1 vector
    s : N x 1 vector with components in {-1;1}
    """
    
    N = np.shape(h)[0]
    E = -1/2*np.dot(np.transpose(s),np.dot(J,s)) + np.dot(np.transpose(s),h)
    return E[0]

def optimalIsingEnergyBruteForce(J,h):

    """
    Compute the ground state minimizing the Ising energy for a given matrix J and a given vector h, using brute force method.

    J : N x N matrix
    h : N x 1 vector
    """

    N = np.shape(h)[0]
    sample = allBinaryCombinations(N)
    lowestEnergy = float('inf')
    bestGroundState = []
    for combination in sample:
        groundState = binaryToSpin(combination)
        E = IsingEnergy(J,h,groundState)
        if E < lowestEnergy:
            lowestEnergy = E
            bestGroundState = groundState
    return lowestEnergy, bestGroundState  


"""J = np.array([[1,0,0],[0,1,0],[0,0,1]])    
h = np.array([[1],[1],[1]])
h = np.zeros((3,1))
v = np.array([[100],[100],[100]])    

print(optimalIsingEnergyBruteForce(J,h))
print(IsingEnergy(J,h,v))"""