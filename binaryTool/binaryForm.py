import numpy as np

def toBinary(v,W):

    """
    Convertit un vecteur de n entiers en le vecteur de W*n bits correspondant.
    L'ordre des bits est poids faible -> poids fort.

    v : n x 1 vecteur
    W : int -> nombre de bits par entier
    """
    
    N = np.shape(v)[0]
    wBinary = np.zeros((N*W,1))


    for i in range(N):

        binaryDecomposition = []
        n = v[i][0]

        if n < 0 :
            raise Exception("The components of v must be postive. The {}th was negative and equal to {}".format(i+1,n))

        elif int(np.log(n)/np.log(2))+1 > W :
            raise Exception("Les composantes de v ne doivent pas avoir une décmposition binaire de plus de {} chiffres".format(W))

        else:

            while(n>0):
                bit = n%2
                binaryDecomposition.append(bit)
                n = n//2

            binaryDecomposition += [0 for j in range(W-len(binaryDecomposition))]

            for k in range(W):
                wBinary[i*W + k][0] = binaryDecomposition[k]

    return wBinary

def binaryVectorExtension(v,W):

    """
    Etend un vecteur dans la "nouvelle base" de représentation binaire.

    v : vecteur
    W : int -> nombre de bits par entier
    """ 

    n = np.shape(v)[0]
    vHat = np.zeros((n*W,1))

    for k in range(n):
        for l in range(W):
            vHat[k*W+l] = 2**l * v[k][0]

    return vHat  

def binaryMatrixExtension(M,W):

    """
    Etend une matrice dans la "nouvelle base" de représentation binaire.

    M : matrice symmétrique
    W : int -> nombre de bits par entier
    """ 

    n,m = np.shape(M)
    if (n != m):
        raise Exception("La matrice n'est pas carrée !")
    MHat = np.zeros((n*W,n*W))

    for k in range(W):
        for l in range(W):
            for i in range(n):
                for j in range(n):
                    MHat[k*n+i][l*n+j] = 2**(k+l)*M[i][j]

    return MHat                


#w = np.array([[4],[5],[2],[1]])     
#print(toBinary(w,7))       

#print(binaryVectorExtension(np.array([[1],[2],[3]]),5))

#M = np.array([[1,2],[3,4]])
#print(binaryMatrixExtension(M,10))