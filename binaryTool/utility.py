import numpy as np

def allBinaryCombinations(N):

    combinations = []

    for i in range(2**N):
        binaryDecomposition = []
        n = i
        while(n>0):
            bit = n%2
            binaryDecomposition.append(bit)
            n = n//2
            binaryDecomposition.reverse()
        binaryDecomposition = [0 for j in range(N-len(binaryDecomposition))] + binaryDecomposition
        binaryArray = np.zeros((N,1))
        for i in range(N):
            binaryArray[i][0] = binaryDecomposition[i]
        combinations.append(binaryArray) 

    return combinations 

def binaryToSpin(binaryDecomposition):
    return 2*binaryDecomposition - 1  

def spinToBinary(groundState):
    return (groundState + 1)/2     

def sign(x):
    if x < 0:
        return -1
    elif x == 0:
        return 0
    else:    
        return 1     

def nonZeroSign(vecteur):
    n = np.shape(vecteur)[0]
    nonZero = True
    for i in range(n):
        nonZero = nonZero and (sign(vecteur[i][0]) != 0)  
    return nonZero    

def nonDiagonalTerms(M):
    n = np.shape(M)[0]
    elts = []
    for i in range(n):
        for j in range(n):
            if i != j:
                elts.append(M[i][j]) 
    return elts                     