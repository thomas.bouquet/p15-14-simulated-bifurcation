# Importation des modules

import numpy as np
from binaryTool.binaryForm import toBinary, binaryMatrixExtension, binaryVectorExtension
from IsingEnergy import IsingEnergy, optimalIsingEnergyBruteForce

# Définition des variables du problème

"""w = np.array([[4],[5],[2]]) # Vecteur de pondération des actifs
mu = np.array([[-30],[-50],[10]])
sigma = np.array([[-1,-6,4],[-6,0,1],[4,1,2]]) # Matrice symmétrique des covariances entre secteurs des actifs
cost = 0 # Coût d'une transaction"""

def toIsing(sigma,mu,W,gamma):

    """
    Permet de passer du problème défini pour des actifs entiers au problème équivalent définit avec la représentation binaire de ces derniers.

    W : int -> nombre de bits maximum pour la représentation des entiers.
    """

    n = np.shape(sigma)[0]
    U = np.ones((n*W,1))
    sigmaHat = binaryMatrixExtension(sigma,W)
    muHat = binaryVectorExtension(mu,W)
    J = -gamma/2*sigmaHat
    H = gamma/2*np.dot(sigmaHat,U)-muHat

    return J,H

#J,H = toIsing(w,sigma,mu,3,1)
#print(optimalIsingEnergyBruteForce(J,H))

