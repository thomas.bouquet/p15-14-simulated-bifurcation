import parametres as pr 
import numpy as np 
from random import random, randint
from toIsingProblem import toIsing
from binaryTool.utility import nonZeroSign, sign

X = np.zeros((pr.N,1))
Y = np.zeros((pr.N,1))
X_1 = X
Y_1 = Y

if pr.Y0_random:
    for i in range(pr.N):
        Y[i][0] = (2*randint(0,1)-1)*random()/100

"""for timeStep in range(1,pr.nbPoints):
    for i in range(pr.N):
        X[i][timeStep] = X[i][timeStep-1] + pr.delta*Y[i][timeStep-1]*pr.delta_t
    for i in range(pr.N):    
        Y[i][timeStep] = Y[i][timeStep-1] - (pr.K*X[i][timeStep]**3 + (pr.delta-pr.p(T[timeStep]))*X[i][timeStep] - pr.xi0*sum(pr.J[i][j]*X[j][timeStep] for j in range(pr.N)) + 2*pr.A(T[timeStep])*pr.xi0*pr.h[i][0])*pr.delta_t
"""

timeStep = 1

x_t = [[] for i in range(pr.N)]
for i in range(pr.N):
    x_t[i].append(0)

#while(not(nonZeroSign(X))):
for timeStep in range(1,pr.nbPoints):
    for i in range(pr.N):
        X[i][0] = X_1[i][0] + (pr.delta+pr.p(timeStep*pr.delta_t))*Y_1[i][0]*pr.delta_t
        x_t[i].append(X[i][0])
    for i in range(pr.N):    
        Y[i][0] = Y_1[i][0] - (pr.K*X[i][0]**3 + (pr.delta-pr.p(timeStep*pr.delta_t))*X[i][0] - pr.xi0*sum(pr.J[i][j]*X[j][0] for j in range(pr.N)) + 2*pr.A(timeStep*pr.delta_t)*pr.xi0*pr.h[i][0])*pr.delta_t
    #timeStep += 1
    X_1 = X
    Y_1 = Y