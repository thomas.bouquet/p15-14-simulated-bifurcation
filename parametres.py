import numpy as np
from toIsingProblem import toIsing
from binaryTool.utility import nonDiagonalTerms
from statistics import stdev

# Paramètres du problème

mu = np.array([[1],[21],[4],[10]]) # Espérance des actifs
sigma = np.array([[1,-0.5,0,-1],[-0.5,1,0.2,0.7],[0,0.2,1,0],[-1,0.7,0,1]]) # Matrice symmétrique des covariances entre secteurs des actifs avec des valeurs entre -1 et 1

n = np.shape(mu)[0] # Nombre d'actifs

C = 28 # Borne de la somme des wi -> optimal si multiple de n et si s'écrit de la forme 2^k-1

# Paramètres liés à la simulation

W = int(np.log(C/n)/np.log(2))+1 # Nombre de bits de représentation pour les entiers dépendnat de la borne uniforme C/n
N = n*W # Nombre de variables après "binairisation" du problème
gamma = 1 # Coefficient de risque

U = np.ones((N,1)) # Vecteur unitaire de même taille que le portefeuille binaire

J,h = toIsing(sigma,mu,W,gamma)

Y0_random = False # Choisit si les y_i sont aléatoires à l'instant initial 

# Paramètres liés à l'Hamiltonien

#lambda_max = max(np.linalg.eig(J)[0]).real # Plus grande valeur propre de J

K = 1 # Constante de Kerr
delta = 1 # Fréquence de detuning
standardDeviation = stdev(nonDiagonalTerms(J)) # Déviation standard des éléments non-diagonaux de J
# xi0 = abs(delta*lambda_max/(4*N)) # Doit être > 0 nécessairement
xi0 = 0.7*delta/(standardDeviation*N**(1/2)) # Doit être > 0 nécessairement

p_th = 5*K
#p_th = delta-xi0*lambda_max

Tmaxp = 200 # Temps en secondes pour que p atteigne sa valeur maximale

delta_t = 0.01 # Pas de temps en seconde
nbPoints = int(Tmaxp/delta_t) # Nombre de points de temps à évaluer

def p(t): # Pression
    #return 5*K*t/10
    return p_th*t/Tmaxp

def A(t): 
    if p(t) < delta:
        return 0
    else:
        return ((p(t)-delta)/K)**(1/2) 