import parametres as pr
from Euler import X,x_t
import numpy as np
from binaryTool.utility import sign, spinToBinary
from IsingEnergy import IsingEnergy, optimalIsingEnergyBruteForce
import matplotlib.pyplot as plt

spinSolution = np.zeros((pr.N,1))
for i in range(pr.N):
    spinSolution[i][0] = sign(X[i][0])
binarySolution = spinToBinary(spinSolution)

solution = np.zeros((pr.n,1))
for i in range(pr.n):
    solution[i][0] = sum(2**k*binarySolution[i*pr.W+k][0] for k in range(pr.W))

#energieIsing = IsingEnergy(-pr.gamma*pr.sigma,-pr.mu,solution)    

print(solution)

# Affichage de l'évolution temporelle du portefeuille  
       
"""for i in range(pr.n):
    entiers = [sum(2**k*int((1+sign(x_t[i*pr.W+k][time]))/2) for k in range(pr.W)) for time in range(len(x_t[0]))]
    plt.plot(list(range(len(x_t[0]))),entiers,label="w"+str(i+1))
    plt.legend()
    plt.grid()
plt.show()"""

for i in range(pr.N):
    plt.plot(list(range(len(x_t[0]))),x_t[i],label="x"+str(i+1))
    plt.legend()
    plt.grid()
plt.show()

### Comparaison à la solution optimale (à décommenter pour utiliser -> uniquement pour des problèmes de petite dimension)
"""
Emin = float('inf')
imin,jmin,kmin,mmin = 0,0,0,0

for i in range(2**pr.W):
    for j in range(2**pr.W):
        for k in range(2**pr.W):
            for m in range(2**pr.W):
                vecteur = np.zeros((4,1))
                vecteur[0][0] = i
                vecteur[1][0] = j
                vecteur[2][0] = k
                vecteur[3][0] = m
                E = IsingEnergy(-1*pr.gamma*pr.sigma,-1*pr.mu,vecteur)
                if E < Emin:
                    Emin = E
                    imin,jmin,kmin,mmin = i,j,k,m
print(Emin,imin,jmin,kmin,mmin)   
"""

E,gs = optimalIsingEnergyBruteForce(pr.J,pr.h)
bestBinarySolution = spinToBinary(gs)
bestSolution = np.zeros((pr.n,1))
for i in range(pr.n):
    bestSolution[i][0] = sum(2**k*bestBinarySolution[i*pr.W+k][0] for k in range(pr.W))
print(bestSolution)    