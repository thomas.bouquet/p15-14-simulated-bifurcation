import matplotlib.pyplot as plt
import numpy as np
import SBparameters.parameters as pr

def plot_timewise_evolution(x_t,portofolio):

    """
    Plot the timewise evolution of each spin of the Ising problem.

    Args:
    *x_t ([float]): list of xi's values' timewise evolution
    *portofolio ([float/int]): assets spreading

    Returns: None
    """

    # Creating the grid according the number of assets

    scale = pr.n**0.5
    if scale != int(scale):
        scale += 1 
    scale = int(scale)    

    # Plotting timewise evolution for each asset

    for i in range(pr.n):

        plt.subplot(scale,scale,i+1)

        for j in range(pr.W): 
            plt.plot(np.linspace(0,pr.Tmaxp,pr.nbPoints),x_t[i*pr.W+j],label="$x_{}{}{}$".format('{',i*pr.W+j+1,'}'))

        plt.legend()
        plt.grid()

        plt.title("Timewise evolution of $x_i$s for the asset $w_{} = {}$".format(i+1,portofolio[i]))    
    
    plt.show()  