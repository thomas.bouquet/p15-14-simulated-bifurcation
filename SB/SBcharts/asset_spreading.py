import matplotlib.pyplot as plt
import SBparameters.parameters as pr

def make_autopct(values):
    def my_autopct(pct):
        total = sum(values)
        val = int(round(pct*total/100.0))
        return '{p:.2f}%  ({v:d})'.format(p=pct,v=val)
    return my_autopct   

def draw_pie(portofolio):

    """
    Draw the pie chart representing the spreading of the assets.

    Args: portofolio ([float/int]): assets spreading

    Returns: None
    """    

    pie_labels = ["Asset $w_{}$".format(i+1) for i in range(pr.n)]
    pie_explode = [0.05 for i in range(pr.n)]    

    plt.pie(portofolio, labels=pie_labels, autopct=make_autopct(portofolio), pctdistance=0.85, explode=pie_explode) 

    centre_circle = plt.Circle((0,0),0.70,fc='white')
    fig = plt.gcf()
    fig.gca().add_artist(centre_circle)

    title = plt.text(0,0,"Total investment",color="black")
    fig.gca().add_artist(title)
    assets_number = plt.text(-10,0,"{} out of {}".format(int(sum(portofolio)),pr.C),color="black")
    fig.gca().add_artist(assets_number)
    assets_percentage = plt.text(-10,-10,"({} percent of available assets)".format(sum(portofolio)/pr.C*100),color="black")
    fig.gca().add_artist(assets_percentage)

    plt.axis('equal')
    plt.tight_layout()

    #plt.title("Assets spreading: total investment of {} out of {} ({} percent of available assets)".format(int(sum(portofolio)),pr.C,sum(portofolio)/pr.C*100))
    plt.show()