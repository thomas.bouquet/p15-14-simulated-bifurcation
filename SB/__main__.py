from SBcharts.asset_spreading import draw_pie
from SBcharts.timewise_evolution import plot_timewise_evolution
from SBeuler.yield_portofolio import yield_portofolio

# Retreiving the results from QSES (Quick Symplectic Euler Scheme)

x_t,portofolio,benefits,simulation_time = yield_portofolio()

print("The optimal portofolio is {} leading to benefits of {}. (Simulation run in {} sec.)".format(str(portofolio),benefits,simulation_time))

# 'xi's timewise evolution  

plot_timewise_evolution(x_t,portofolio)

# Pie chart

draw_pie(portofolio)