from time import *
import SBparameters.parametres as pr
from SBbinary.binaryBasis import toIsing
from SBeuler.eulerScheme import eulerScheme
from statistics import mean, stdev

simulationData = {}

for nbActifs in range(2,4):
    for nbBits in range(2,4):

        N = nbActifs*nbBits

        mu = np.random.random_integers(0,10,size=(nbActifs,1))
        sigma = np.ones((nbActifs,nbActifs))
        for i in range(nbActifs):
            for j in range(i+1,nbActifs):
                sigma[i][j] = 2*random()-1
                sigma[j][i] = sigma[i][j]

        h,J = toIsing(sigma,mu,nbBits,pr.gamma) 

        standardDeviation = stdev(nonDiagonalTerms(J))
        xi0 = 0.7*pr.delta/(standardDeviation*N**(1/2))

        # Récupération du résultat issu de la méthode d'Euler

        spinSolution = np.zeros((N,1))
        calcTime = time()
        X,Y,x_t,y_t = eulerScheme(N,bnBits,pr.nbPoints,pr.delta,pr.K,xi0,pr.p,pr.A,pr.delta_t,J,h,pr.Y0_random)

        # Conversion en spins

        for i in range(N):
            spinSolution[i][0] = sign(mean(x_t[i]))

        # Conversion en binaire    

        binarySolution = spinToBinary(spinSolution)

        # Conversion en entiers

        solution = np.zeros((nbActifs,1))

        for i in range(nbActifs):
            solution[i][0] = sum(2**k*binarySolution[i*nbBits+k][0] for k in range(nbBits)) 

        calcTime = time() - calcTime      

        simulationData[str(nbActifs)+" actifs / "+str(nbBits)+"bits"] = {"solution" : solution, "temps" : calcTime, "J" : J, "h" : h}       

f = open("dict.txt","w")
f.write( str(dict) )
f.close()