import numpy as np
from SBbinary.binaryBasis import toIsing
from SBmatrix.matrixTools import nonDiagonalTerms
from statistics import stdev
from random import random

# Paramètres du problème

mu = np.array([[1],[2],[4],[1]]) # Espérance des actifs
sigma = np.array([[1,-0.5,0,-1],[-0.5,1,0.2,0.7],[0,0.2,1,0],[-1,0.7,0,1]]) # Matrice symmétrique des covariances entre secteurs des actifs avec des valeurs entre -1 et 1

n = np.shape(mu)[0] # Nombre d'actifs

mu = np.random.random_integers(0,10,size=(8,1))
sigma = np.ones((np.shape(mu)[0],np.shape(mu)[0]))
for i in range(np.shape(mu)[0]):
    for j in range(i+1,np.shape(mu)[0]):
        sigma[i][j] = 2*random()-1
        sigma[j][i] = sigma[i][j]

n = np.shape(mu)[0] # Nombre d'actifs"""

C = 200 # Borne de la somme des wi -> le résultat sera le même pour tous les éléments de [2^(W-1);2^W-1]

# Paramètres liés à la simulation

W = int(np.log(C/n)/np.log(2))+1 # Nombre de bits de représentation pour les entiers dépendnat de la borne uniforme C/n
N = n*W # Nombre de variables après "binairisation" du problème
gamma = 1 # Coefficient de risque

U = np.ones((N,1)) # Vecteur unitaire de même taille que le portefeuille binaire

J,h = toIsing(sigma,mu,W,gamma)

Y0_random = False # Choisit si les y_i sont aléatoires à l'instant initial 

M = 2 # Paramètre pour la méthode d'Euler symplectique

# Paramètres liés à l'Hamiltonien

K = 1 # Constante de Kerr
delta = 1 # Fréquence de detuning
standardDeviation = stdev(nonDiagonalTerms(J)) # Déviation standard des éléments non-diagonaux de J
xi0 = 0.7*delta/(standardDeviation*N**(1/2)) # Doit être > 0 nécessairement

p_th = 5*K

Tmaxp = 200 # Temps en secondes pour que p atteigne sa valeur maximale

delta_t = 0.02 # Pas de temps en seconde
nbPoints = int(Tmaxp/delta_t) # Nombre de points de temps à évaluer

def p(t): # Pression
    #return p_th*t/Tmaxp
    return 0.01*t

def A(t): 
    if p(t) < delta:
        return 0
    else:
        return ((p(t)-delta)/K)**(1/2) 