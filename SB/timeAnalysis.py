from time import *
import SBparameters.parametres as pr
from SBbinary.binaryBasis import toIsing, sign, spinToBinary, binaryToSpin
from SBeuler.eulerScheme import eulerScheme
from SBeuler.symplecticEulerScheme import symplecticEulerScheme
from statistics import mean, stdev
import numpy as np
from random import random
from SBmatrix.matrixTools import nonDiagonalTerms
import json
from SBeuler.quickSymplecticEulerScheme import quickSymplecticEulerScheme

simulationData = {}

def boolToBinary(boolean):
    if boolean :
        return 1
    else:
        return 0    

def IsingEnergy(J,h,s):

    """
    Compute the Ising energy for given matrix J, vector h and a ground-state s.

    J : N x N matrix
    h : N x 1 vector
    s : N x 1 vector with components in {-1;1}
    """
    
    N = np.shape(h)[0]
    E = -1/2*np.dot(np.transpose(s),np.dot(J,s)) + np.dot(np.transpose(s),h)
    return E[0]

def optimalIsingEnergyBruteForce(J,h):

    """
    Compute the ground state minimizing the Ising energy for a given matrix J and a given vector h, using brute force method.

    J : N x N matrix
    h : N x 1 vector
    """

    N = np.shape(h)[0]
    sample = allBinaryCombinations(N)
    lowestEnergy = float('inf')
    bestGroundState = []
    for combination in sample:
        groundState = binaryToSpin(combination)
        E = IsingEnergy(J,h,groundState)
        if E < lowestEnergy:
            lowestEnergy = E
            bestGroundState = groundState
    return lowestEnergy, bestGroundState     

def allBinaryCombinations(N):

    combinations = []

    for i in range(2**N):
        binaryDecomposition = []
        n = i
        while(n>0):
            bit = n%2
            binaryDecomposition.append(bit)
            n = n//2
            binaryDecomposition.reverse()
        binaryDecomposition = [0 for j in range(N-len(binaryDecomposition))] + binaryDecomposition
        binaryArray = np.zeros((N,1))
        for i in range(N):
            binaryArray[i][0] = binaryDecomposition[i]
        combinations.append(binaryArray) 

    return combinations              

for loop in range(20):
    for nbBits in range(4,5):

        nbActifs = 5

        N = nbActifs*nbBits

        mu = np.random.random_integers(0,10,size=(nbActifs,1))
        sigma = np.ones((nbActifs,nbActifs))
        for i in range(nbActifs):
            for j in range(i+1,nbActifs):
                sigma[i][j] = 2*random()-1
                sigma[j][i] = sigma[i][j]

        J,h = toIsing(sigma,mu,nbBits,pr.gamma) 

        standardDeviation = stdev(nonDiagonalTerms(J))
        xi0 = 0.7*pr.delta/(standardDeviation*N**(1/2))

        # Récupération du résultat issu de la méthode d'Euler

        spinSolution = np.zeros((N,1))
        calcTime = time()
        #X,Y,x_t,y_t = eulerScheme(N,nbBits,pr.nbPoints,pr.delta,pr.K,xi0,pr.p,pr.A,pr.delta_t,J,h,pr.Y0_random)
        X,Y,x_t,y_t = quickSymplecticEulerScheme(N,nbBits,pr.nbPoints,pr.delta,pr.K,xi0,pr.p,pr.A,pr.delta_t,J,h,pr.Y0_random,pr.M)

        # Conversion en spins

        for i in range(N):
            spinSolution[i][0] = sign(mean(x_t[i]))

        # Conversion en binaire    

        binarySolution = spinToBinary(spinSolution)

        # Conversion en entiers

        solution = np.zeros((nbActifs,1))

        for i in range(nbActifs):
            solution[i][0] = sum(2**k*binarySolution[i*nbBits+k][0] for k in range(nbBits)) 

        calcTime = time() - calcTime    

        SBsol = [solution[i][0] for i in range(nbActifs)] 

        timeOpti = time()

        E,gs = optimalIsingEnergyBruteForce(J,h)
        bestBinarySolution = spinToBinary(gs)
        bestSolution = np.zeros((nbActifs,1))
        for i in range(nbActifs):
            bestSolution[i][0] = sum(2**k*bestBinarySolution[i*nbBits+k][0] for k in range(nbBits))

        timeOpti = time() - timeOpti    

        BFsol = [bestSolution[i][0] for i in range(nbActifs)] 

        vn,vm = np.shape(solution)
        equals = np.equal(solution,bestSolution)
        listeEqual = []
        for i in range(vn):
            listeEqual.append(boolToBinary(equals[i][0]))

        simulationData[str(nbActifs)+" actifs / "+str(nbBits)+"bits - essai #"+str(loop+1)]  = {"tempsSB" : calcTime, "tempsBF" : timeOpti, "identiques" : listeEqual, "QSES": SBsol, "BF": BFsol, "similarite": abs(sum(BFsol[i]*BFsol[i] for i in range(nbActifs))-sum(SBsol[i]*BFsol[i] for i in range(nbActifs)))/sum(BFsol[i]*BFsol[i] for i in range(nbActifs))*100}  

        #simulationData[str(nbActifs)+" actifs / "+str(nbBits)+"bits"] = {"solution" : solution, "temps" : calcTime, "J" : J, "h" : h}       

json = json.dumps(simulationData)
f = open("dataQSES2.json","w")
f.write(json)
f.close()