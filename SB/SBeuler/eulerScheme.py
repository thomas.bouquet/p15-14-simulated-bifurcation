import numpy as np 
from random import random, randint

def eulerScheme(N,W,nbPoints,delta,K,xi0,p,A,delta_t,J,h,Y0_random):

    X = np.zeros((N,1)) # Vecteur des xi
    Y = np.zeros((N,1)) # Vecteur des yi
    X_1 = X # Vecteur des xi à l'instant n-1
    Y_1 = Y # Vecteur des yi à l'instant n-1

    if Y0_random:
        for i in range(N):
            Y[i][0] = (2*randint(0,1)-1)*random()/100

    x_t = [[] for i in range(N)]
    y_t = [[] for i in range(N)]

    for i in range(N):
        x_t[i].append(0)
        y_t[i].append(0)

    for timeStep in range(1,nbPoints):
        
        for i in range(N):
            X[i][0] = X_1[i][0] + (delta+p(timeStep*delta_t)-xi0*J[i][i])*Y_1[i][0]*delta_t
            x_t[i].append(X[i][0])

        for i in range(N):    
            Y[i][0] = Y_1[i][0] - (K*X[i][0]**3 + (delta-p(timeStep*delta_t))*X[i][0] - xi0*sum(J[i][j]*X[j][0] for j in range(N)) + 2*A(timeStep*delta_t)*xi0*h[i][0])*delta_t
            y_t[i].append(Y[i][0])

        X_1 = X
        Y_1 = Y

    return X,Y,x_t,y_t    