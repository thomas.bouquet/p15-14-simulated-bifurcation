import numpy as np 
from random import random, randint
import SBparameters.parameters as pr
import time

def quick_symplectic_Euler_scheme():

    dt = pr.delta_t/pr.M # Symplectic time step
    I = np.eye(pr.N) # Identity matrix

    X = np.zeros((pr.N,1)) # xi's vector
    Y = np.zeros((pr.N,1)) # yi's vector

    if pr.Y0_random:
        for i in range(pr.N):
            Y[i][0] = (2*randint(0,1)-1)*random()/100

    x_t = [[] for i in range(pr.N)]
    y_t = [[] for i in range(pr.N)]

    for i in range(pr.N):
        x_t[i].append(0)
        y_t[i].append(0)

    diagJ = np.zeros(np.shape(pr.J))
    
    for i in range(np.shape(pr.J)[0]):
        diagJ[i][i] = pr.J[i][i]

    start_time = time.time()

    for timeStep in range(1,pr.nbPoints):

        for j in range(1,pr.M+1):

            X += dt*((pr.delta+pr.p(timeStep*pr.delta_t)*I - pr.xi0*diagJ) @ Y)
            Y -= dt*(X**3 + (pr.delta-pr.p(timeStep*pr.delta_t))*X)  

        Y += (pr.xi0*pr.J@X- 2*pr.xi0*pr.A(timeStep*pr.delta_t)*pr.h)*pr.delta_t

        for i in range(pr.N):
            x_t[i].append(X[i][0])
            y_t[i].append(Y[i][0])

    end_time = time.time() 

    simulation_time = end_time - start_time      

    return X,Y,x_t,y_t,simulation_time  