import matplotlib.pyplot as plt
import numpy as np
from statistics import mean
import SBparameters.parameters as pr
from SBbinary.binaryBasis import sign, spinToBinary
from SBeuler.quick_symplectic_Euler_scheme import quick_symplectic_Euler_scheme 

def yield_portofolio():

    # Initialization

    spinSolution = np.zeros((pr.N,1))
    solution = np.zeros((pr.n,1))
    portofolio = []

    # Retreiving results from QSES

    X,Y,x_t,y_t,simulation_time = quick_symplectic_Euler_scheme()#pr.N,pr.W,pr.nbPoints,pr.delta,pr.K,pr.xi0,pr.p,pr.A,pr.delta_t,pr.J,pr.h,pr.Y0_random,pr.M)

    for i in range(pr.N):
        spinSolution[i][0] = sign(mean(x_t[i]))

    binarySolution = spinToBinary(spinSolution)     

    for i in range(pr.n):
        solution[i][0] = sum(2**k*binarySolution[i*pr.W+k][0] for k in range(pr.W))
        portofolio.append(solution[i][0])

    # Computation of benefits
    
    benefits = (np.dot(np.transpose(solution),pr.mu)-pr.gamma/2*np.dot(np.transpose(solution),np.dot(pr.sigma,solution)))[0]

    return(x_t,portofolio,benefits,simulation_time) 