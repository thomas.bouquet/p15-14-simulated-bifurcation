import numpy as np

def nonDiagonalTerms(M):
    n = np.shape(M)[0]
    elts = []
    for i in range(n):
        for j in range(n):
            if i != j:
                elts.append(M[i][j]) 
    return elts   