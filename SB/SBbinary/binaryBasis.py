import numpy as np

def toBinary(v,W):

    """
    Convertit un vecteur de n entiers en le vecteur de W*n bits correspondant.
    L'ordre des bits est poids faible -> poids fort.

    v : n x 1 vecteur
    W : int -> nombre de bits par entier
    """
    
    N = np.shape(v)[0]
    wBinary = np.zeros((N*W,1))


    for i in range(N):

        binaryDecomposition = []
        n = v[i][0]

        if n < 0 :
            raise Exception("Les composantes du vecteur doivent être positives. La {}ème est négative et égale à {}".format(i+1,n))

        elif int(np.log(n)/np.log(2))+1 > W :
            raise Exception("Les composantes de v ne doivent pas avoir une décmposition binaire de plus de {} chiffres, ce qui n'est pas le cas de la {}ème.".format(W,i+1))

        else:

            while(n>0):
                bit = n%2
                binaryDecomposition.append(bit)
                n = n//2

            binaryDecomposition += [0 for j in range(W-len(binaryDecomposition))]

            for k in range(W):
                wBinary[i*W + k][0] = binaryDecomposition[k]

    return wBinary

def binaryVectorExtension(v,W):

    """
    Etend un vecteur dans la "nouvelle base" de représentation binaire.

    v : vecteur
    W : int -> nombre de bits par entier
    """ 

    n = np.shape(v)[0]
    vHat = np.zeros((n*W,1))

    for k in range(n):
        for l in range(W):
            vHat[k*W+l] = 2**l * v[k][0]

    return vHat  

def binaryMatrixExtension(M,W):

    """
    Etend une matrice dans la "nouvelle base" de représentation binaire.

    M : matrice symmétrique
    W : int -> nombre de bits par entier
    """ 

    n,m = np.shape(M)

    if (n != m):
        raise Exception("La matrice n'est pas carrée !")
    
    MHat = np.zeros((n*W,n*W))

    for k in range(W):
        for l in range(W):
            for i in range(n):
                for j in range(n):
                    MHat[k*n+i][l*n+j] = 2**(k+l)*M[i][j]

    return MHat    

def toIsing(sigma,mu,W,gamma):

    """
    Permet de passer du problème défini pour des actifs entiers au problème équivalent définit avec la représentation binaire de ces derniers.

    W : int -> nombre de bits maximum pour la représentation des entiers.
    """

    n = np.shape(sigma)[0]
    U = np.ones((n*W,1))
    sigmaHat = binaryMatrixExtension(sigma,W)
    muHat = binaryVectorExtension(mu,W)
    J = -gamma/2*sigmaHat
    H = gamma/2*np.dot(sigmaHat,U)-muHat

    return J,H      

def binaryToSpin(binaryDecomposition):

    """
    Convertit un vecteur de 0 et de 1 en vecteur de -1 et de 1.
    """

    return 2*binaryDecomposition - 1  

def spinToBinary(groundState):

    """
    Convertit un vecteur de -1 et de 1 en vecteur de 0 et de 1.
    """
    
    return (groundState + 1)/2   

def sign(x):
    if x < 0:
        return -1
    elif x == 0:
        return 0
    else:    
        return 1             