# P15-14 Simulated Bifurcation

Implementation of the Simulated Bifurcation Algorithm to solve the Optimal Trading Trajectory Problem.

CentraleSupélec - Pôle P15 : Modélisation mathématique des systèmes complexes

# Mode d'emploi

Les paramètres du problème sont fixés dans le fichier parametres.py : il faut donc directement les modifier dans ce fichier.
Ensuite il suffit d'exécuter le fichier simulation.py pour obtenir la solution.

Cette solution est calculée via la méthode d'Euler définie dans le fichier Euler.py ; ce qui est affiché dans la console correspond au portefeuille optimal obtenu, avec l'énergie d'Ising associée. L'évolution temporelle de ses composantes est tracée pour en montrer l'évolution.